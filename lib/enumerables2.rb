require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr.reduce(0) { |sum, el| sum += el }
  # solution: arr.reduce(:+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_string? helper method
def in_all_strings?(long_strings, substring)
  long_strings.each do |long_string|
    return false unless sub_string?(long_string, substring)
  end
  true
end
#solution: long_strings.all? { | long_string | sub_string?(long_string, substring)}
# or just use String#include

def sub_string?(long_string, substring)
  long_string.index(substring) != nil
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  unsorted_non_unique_letters(string).sort!
end

def unsorted_non_unique_letters(string)
  string.chars.reduce([]) do |repetitves, letter|
    if letter >= 'a' && letter <= 'z' && string.count(letter) > 1 && !repetitves.include?(letter)
      repetitves << letter
    else
      repetitves
    end
  end
end
#solution: get the unique letters, then count them, select the count > 1 ones

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  words = string.split.sort_by { |word| word.length}
  words.reverse!
  words[0, 2] #[start, length]
  # or words[-2..-1]
  # solution: string.delete!(",.;:!?")
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  ('a'..'z').reduce([]) do |missing, letter|
    if !string.include?(letter)
      missing << letter
    else
      missing
    end
  end
end
# use reject


# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr .. last_yr).select { |year| not_repeat_year?(year)}
end

def not_repeat_year?(year)
  year_array = year.to_s.chars
  year_array == year_array.uniq
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  #first select unique songs!!!
  uniq_songs = songs.uniq
  uniq_songs.select { |song| no_repeats?(song, songs) }
end

def no_repeats?(song_name, songs)
  songs.each_index do |week|
    if song_name == songs[week]
      return false if songs[week] == songs[week + 1]
    end
  end
  true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  string = remove_punctuation(string)
  sorted = string.split.sort_by { |word| c_distance(word) }
  sorted[0]
  # solution: directly use .first
  # check edge cases with no "c"
end

def remove_punctuation(string)
  string.gsub(/[^a-z0-9\s]/i, '') #regex!!!
  # or string.delete!(",.;:!?")
end

def c_distance(word)
  return 100 if !word.include?("c")
  word.length - word.reverse.index("c") - 1

end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

# solution
# def repeated_number_ranges(numbers)
#   ranges = []
#   start_index = nil
#
#   # start walking
#   # set the start_index when we're at the beginning of a range
#   # when we reach the end of a range, add the range to the list and reset the start_index
#
#   numbers.each_with_index do |el, idx|
#     next_el = numbers[idx + 1]
#     if el == next_el
#       start_index = idx unless start_index #i.e., reset the start_index if it's nil
#     elsif start_index # i.e., if the start index isn't nil (the numbers switched)
#       ranges.push([start_index, idx])
#       start_index = nil # reset the start_index to nil so we can capture more ranges
#     end
#   end
#
#   ranges
# end

def repeated_number_ranges(arr)
  result = []
  start_index = nil

  arr.each_index do |index|
    if arr[index] == arr[index + 1]
      start_index = index unless start_index
    elsif start_index
      result.push([start_index, index])
      start_index = nil
    end
  end

  result
end
